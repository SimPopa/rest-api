import * as http from 'http';
import { parse, UrlWithParsedQuery } from 'url';
import { createUser, getUsers, deleteUser } from './api/users';
import { getUserData, getUserHobbies, updateUserHobbies } from './api/hobbies';
import { parseRequestBody } from './utils/handlers';

const PORT = 3000;
const hobbiesPath = /^\/api\/users\/([a-f\d]{8}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{12})\/hobbies$/i;

const server: http.Server = http.createServer(async (req: http.IncomingMessage, res: http.ServerResponse) => {
    const { pathname, query }: UrlWithParsedQuery = parse(req.url!, true);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

    if (req.method === 'OPTIONS') {
        res.writeHead(200);
        res.end();
        return;
    }

    const sendResponse = (statusCode: number, data: any, error: string | null = null,  cacheControl: string = 'no-store') => {
        res.writeHead(statusCode, { 'Content-Type': 'application/json',  'Cache-Control': cacheControl });
        res.end(JSON.stringify({ data, error }));
    };

    if (pathname === '/api/users' && req.method === 'POST') {
        try {
            const newUser = await createUser(req);
            const response = {
                user: newUser,
                links: {
                    self: `/api/users/${newUser.id}`,
                    hobbies: `/api/users/${newUser.id}/hobbies`
                }
            };
            sendResponse(201, response);
        } catch (error) {
            console.error(error);
            sendResponse(500, null, (error as Error).message);
        }
    } else if (pathname === '/api/users' && req.method === 'GET') {
        try {
            const userList = await getUsers();
            const responseData = userList.map(user => ({
                user,
                links: {
                    self: `/api/users/${user.id}`,
                    hobbies: `/api/users/${user.id}/hobbies`
                }
            }));
            sendResponse(200, responseData, null, 'public, max-age=3600');
        } catch (error) {
            console.error(error);
            sendResponse(500, null, (error as Error).message);
        }
    } else if (pathname !== null && pathname.startsWith('/api/users/') && req.method === 'DELETE') {
        const userId: string = pathname.split('/')[3];
        try {
            await deleteUser(userId);
            sendResponse(200, { success: true });
        } catch (error) {
            sendResponse(500, null, `User with id ${userId} doesn't exist`);
        }
    } else if (pathname !== null && pathname.match(hobbiesPath) && req.method === 'GET') {
        const userId: string = pathname.split('/')[3];;
        try {
            const userHobbies = await getUserHobbies(userId);
            const hobbiesresponse = {
                hobbies: userHobbies,
                links: {
                    self: `/api/users/${userId}/hobbies`,
                    user: `/api/users/${userId}`
                }
            };
            sendResponse(200, hobbiesresponse, null, 'private, max-age=3600');
        } catch (error) {
            sendResponse(500, null, (error as Error).message);
        }
    } else if (pathname !== null && pathname.match(hobbiesPath) && req.method === 'PATCH') {
        const userId: string = pathname.match(hobbiesPath)![1];
        try {
            const requestBody = await parseRequestBody(req);
            await updateUserHobbies(userId, requestBody.hobbies);
            const userData = await getUserData(userId);
            const response = {
                user: userData,
                links: {
                    self: `/api/users/${userId}`,
                    hobbies: `/api/users/${userId}/hobbies`
                }
            };
            sendResponse(200, response);
        } catch (error) {
            sendResponse(500, null, (error as Error).message);
        }
    } else {
        sendResponse(404, null, 'Not Found');
    }
});

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});