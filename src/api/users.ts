import { v4 as uuidv4 } from 'uuid';
import http from 'http';

import { User } from '../utils/models';
import { parseRequestBody } from '../utils/handlers';

let users: User[] = [];

const generateUUID = (): string => {
	return uuidv4();
};

export const createUser = async (req: http.IncomingMessage): Promise<any> => {
	try {
		const requestBody = await parseRequestBody(req);
		const newUser = {
			id: generateUUID(),
			name: requestBody.name,
			email: requestBody.email
		};
		users.push(newUser);
		return newUser;
	} catch (error) {
		throw new Error('Failed to create user');
	}
};

export const getUsers = async (): Promise<any[]> => {
	return users;
};

export const deleteUser = async (userId: string): Promise<any> => {
	const index = users.findIndex(user => user.id === userId);
	if (index !== -1) {
		const deletedUser = users.splice(index, 1)[0];
		return deletedUser;
	} else {
		throw new Error('User not found');
	}
};

export const findUser = async (userId: string): Promise<any> => {
	const index = users.findIndex(user => user.id === userId);
	if (index > -1) {
		return users[index];
	} else {
		throw new Error('User not found');
	}
}