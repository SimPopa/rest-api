import { User } from '../utils/models';
import{ findUser } from './users';

let hobbies: { [userId: string]: string[] } = {};

export const getUserData = async (userId: string): Promise<User> => {
    if (hobbies[userId]) {
        const userData = findUser(userId)
        return userData;
    } else {
        throw new Error('Hobbies not found for the user');
    }
};

export const getUserHobbies = async (userId: string): Promise<string[]> => {
    if (hobbies[userId]) {
        return hobbies[userId];
    } else {
        throw new Error('Hobbies not found for the user');
    }
}

export const updateUserHobbies = async (userId: string, newHobbies: string[]): Promise<string[]> => {
    if (hobbies[userId]) {
        const updatedHobbies = Array.from(new Set([...hobbies[userId], ...newHobbies]));
        hobbies[userId] = updatedHobbies;
    } else {
        hobbies[userId] = newHobbies;
    }
    return hobbies[userId];
};
