export interface User {
  id: string,
  name: string,
  email: string,
  hobbies?: string[]

}

export interface Hobbies {
  [userId: string]: string[]
}
export interface Links {
    self: string,
    hobbies: Hobbies
}
