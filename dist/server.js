"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const http = __importStar(require("http"));
const url_1 = require("url");
const users_1 = require("./api/users");
const hobbies_1 = require("./api/hobbies");
const handlers_1 = require("./utils/handlers");
const PORT = 3000;
const hobbiesPath = /^\/api\/users\/([a-f\d]{8}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{12})\/hobbies$/i;
const server = http.createServer(async (req, res) => {
    const { pathname, query } = (0, url_1.parse)(req.url, true);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    if (req.method === 'OPTIONS') {
        res.writeHead(200);
        res.end();
        return;
    }
    const sendResponse = (statusCode, data, error = null, cacheControl = 'no-store') => {
        res.writeHead(statusCode, { 'Content-Type': 'application/json', 'Cache-Control': cacheControl });
        res.end(JSON.stringify({ data, error }));
    };
    if (pathname === '/api/users' && req.method === 'POST') {
        try {
            const newUser = await (0, users_1.createUser)(req);
            const response = {
                user: newUser,
                links: {
                    self: `/api/users/${newUser.id}`,
                    hobbies: `/api/users/${newUser.id}/hobbies`
                }
            };
            sendResponse(201, response);
        }
        catch (error) {
            console.error(error);
            sendResponse(500, null, error.message);
        }
    }
    else if (pathname === '/api/users' && req.method === 'GET') {
        try {
            const userList = await (0, users_1.getUsers)();
            const responseData = userList.map(user => ({
                user,
                links: {
                    self: `/api/users/${user.id}`,
                    hobbies: `/api/users/${user.id}/hobbies`
                }
            }));
            sendResponse(200, responseData, null, 'public, max-age=3600');
        }
        catch (error) {
            console.error(error);
            sendResponse(500, null, error.message);
        }
    }
    else if (pathname !== null && pathname.startsWith('/api/users/') && req.method === 'DELETE') {
        const userId = pathname.split('/')[3];
        try {
            await (0, users_1.deleteUser)(userId);
            sendResponse(200, { success: true });
        }
        catch (error) {
            sendResponse(500, null, `User with id ${userId} doesn't exist`);
        }
    }
    else if (pathname !== null && pathname.match(hobbiesPath) && req.method === 'GET') {
        const userId = pathname.split('/')[3];
        ;
        try {
            const userHobbies = await (0, hobbies_1.getUserHobbies)(userId);
            const hobbiesresponse = {
                hobbies: userHobbies,
                links: {
                    self: `/api/users/${userId}/hobbies`,
                    user: `/api/users/${userId}`
                }
            };
            sendResponse(200, hobbiesresponse, null, 'private, max-age=3600');
        }
        catch (error) {
            sendResponse(500, null, error.message);
        }
    }
    else if (pathname !== null && pathname.match(hobbiesPath) && req.method === 'PATCH') {
        const userId = pathname.match(hobbiesPath)[1];
        try {
            const requestBody = await (0, handlers_1.parseRequestBody)(req);
            await (0, hobbies_1.updateUserHobbies)(userId, requestBody.hobbies);
            const userData = await (0, hobbies_1.getUserData)(userId);
            const response = {
                user: userData,
                links: {
                    self: `/api/users/${userId}`,
                    hobbies: `/api/users/${userId}/hobbies`
                }
            };
            sendResponse(200, response);
        }
        catch (error) {
            sendResponse(500, null, error.message);
        }
    }
    else {
        sendResponse(404, null, 'Not Found');
    }
});
server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
