"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleDeleteRequest = exports.handlePutRequest = exports.handlePostRequest = exports.handleGetRequest = exports.sendResponse = exports.CONTENT_TYPE_JSON = exports.CONTENT_TYPE_HTML = exports.users = void 0;
exports.users = [
    { id: 1, name: 'Schmary', email: 'reachSchmary@example.com' },
    { id: 2, name: 'Schmike', email: 'reachSchmike@example.com' },
];
exports.CONTENT_TYPE_HTML = { "Content-Type": "text/html" };
exports.CONTENT_TYPE_JSON = { "Content-Type": "application/json" };
const sendResponse = (res, statusCode, contentType, data) => {
    res.writeHead(statusCode, contentType);
    res.end(JSON.stringify(data));
};
exports.sendResponse = sendResponse;
const handleGetRequest = (req, res, parsedUrl) => {
    if (parsedUrl.path === '/') {
        // Return HTML response for the home page
        (0, exports.sendResponse)(res, 200, exports.CONTENT_TYPE_HTML, `<b>Users <a href = '/user'>list</a> page</b>`);
    }
    else if (parsedUrl.path === '/api/users') {
        // Return JSON response with the list of products
        (0, exports.sendResponse)(res, 200, exports.CONTENT_TYPE_JSON, exports.users);
    }
    else if (parsedUrl.path.startsWith("/api/users")) {
        // Get product by id. A product can be fetched using path param or query param
        const userId = Number(parsedUrl.path.split('/').pop());
        const user = getUserById(userId);
        if (user) {
            // Return JSON response with the product details
            (0, exports.sendResponse)(res, 200, exports.CONTENT_TYPE_JSON, user);
        }
        else {
            // Return a 404 response if the product is not found
            (0, exports.sendResponse)(res, 404, exports.CONTENT_TYPE_JSON, { error: 'User not found' });
        }
    }
    else {
        // Return a 404 response if the endpoint is not found
        (0, exports.sendResponse)(res, 404, exports.CONTENT_TYPE_JSON, { error: 'Endpoint not found' });
    }
};
exports.handleGetRequest = handleGetRequest;
const handlePostRequest = (req, res) => {
    let requestBody = '';
    req.on('data', (chunk) => {
        // Accumulate the request body
        requestBody += chunk;
    });
    req.on('end', () => {
        // Parse the request body and add the new product
        const user = JSON.parse(requestBody);
        user.id = exports.users.length + 1;
        exports.users.push(user);
        // Return JSON response with the newly added product
        (0, exports.sendResponse)(res, 201, exports.CONTENT_TYPE_JSON, user);
    });
};
exports.handlePostRequest = handlePostRequest;
const handlePutRequest = (req, res, parsedUrl) => {
    let requestBody = '';
    req.on('data', (chunk) => {
        // Accumulate the request body
        requestBody += chunk;
    });
    req.on('end', () => {
        // Parse the request body and update the existing product
        const updatedUser = JSON.parse(requestBody);
        const userId = parseInt(parsedUrl.path.split('/').pop());
        const userIndex = exports.users.findIndex(user => user.id === userId);
        if (userIndex !== -1) {
            // Update the product and return JSON response with the updated product
            exports.users[userIndex] = { ...exports.users[userIndex], ...updatedUser, id: userId };
            (0, exports.sendResponse)(res, 200, exports.CONTENT_TYPE_JSON, exports.users[userIndex]);
        }
        else {
            // Return a 404 response if the product is not found
            (0, exports.sendResponse)(res, 404, exports.CONTENT_TYPE_JSON, { error: 'Product not found' });
        }
    });
};
exports.handlePutRequest = handlePutRequest;
const handleDeleteRequest = (req, res, parsedUrl) => {
    const productId = parseInt(parsedUrl.path.split('/').pop());
    const productIndex = exports.users.findIndex(user => user.id === productId);
    if (productIndex !== -1) {
        // Remove the product and return JSON response with the deleted product
        const deletedProduct = exports.users.splice(productIndex, 1)[0];
        (0, exports.sendResponse)(res, 200, exports.CONTENT_TYPE_JSON, deletedProduct);
    }
    else {
        // Return a 404 response if the product is not found
        (0, exports.sendResponse)(res, 404, exports.CONTENT_TYPE_JSON, { error: 'Product not found' });
    }
};
exports.handleDeleteRequest = handleDeleteRequest;
const getUserById = (userId) => {
    return exports.users.find(user => user.id === userId);
};
