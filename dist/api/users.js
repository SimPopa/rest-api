"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findUser = exports.deleteUser = exports.getUsers = exports.createUser = void 0;
const uuid_1 = require("uuid");
const handlers_1 = require("../utils/handlers");
let users = [];
const generateUUID = () => {
    return (0, uuid_1.v4)();
};
const createUser = async (req) => {
    try {
        const requestBody = await (0, handlers_1.parseRequestBody)(req);
        const newUser = {
            id: generateUUID(),
            name: requestBody.name,
            email: requestBody.email
        };
        users.push(newUser);
        return newUser;
    }
    catch (error) {
        throw new Error('Failed to create user');
    }
};
exports.createUser = createUser;
const getUsers = async () => {
    return users;
};
exports.getUsers = getUsers;
const deleteUser = async (userId) => {
    const index = users.findIndex(user => user.id === userId);
    if (index !== -1) {
        const deletedUser = users.splice(index, 1)[0];
        return deletedUser;
    }
    else {
        throw new Error('User not found');
    }
};
exports.deleteUser = deleteUser;
const findUser = async (userId) => {
    const index = users.findIndex(user => user.id === userId);
    if (index > -1) {
        return users[index];
    }
    else {
        throw new Error('User not found');
    }
};
exports.findUser = findUser;
