"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUserHobbies = exports.getUserHobbies = exports.getUserData = void 0;
const users_1 = require("./users");
let hobbies = {};
const getUserData = async (userId) => {
    if (hobbies[userId]) {
        const userData = (0, users_1.findUser)(userId);
        return userData;
    }
    else {
        throw new Error('Hobbies not found for the user');
    }
};
exports.getUserData = getUserData;
const getUserHobbies = async (userId) => {
    if (hobbies[userId]) {
        return hobbies[userId];
    }
    else {
        throw new Error('Hobbies not found for the user');
    }
};
exports.getUserHobbies = getUserHobbies;
const updateUserHobbies = async (userId, newHobbies) => {
    if (hobbies[userId]) {
        const updatedHobbies = Array.from(new Set([...hobbies[userId], ...newHobbies]));
        hobbies[userId] = updatedHobbies;
    }
    else {
        hobbies[userId] = newHobbies;
    }
    return hobbies[userId];
};
exports.updateUserHobbies = updateUserHobbies;
