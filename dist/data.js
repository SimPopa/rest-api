"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let users = [];
module.exports = {
    getUsers: () => users,
    getUserById: (userId) => users.find(user => user.id === userId),
    addUser: (user) => {
        users.push(user);
    },
    deleteUserById: (userId) => {
        users = users.filter(user => user.id !== userId);
    },
    updateUserHobbies: (userId, hobbiesToAdd) => {
        const user = users.find(user => user.id === userId);
        if (!user)
            return;
        const existingHobbies = new Set(user.hobbies);
        hobbiesToAdd.forEach(hobby => existingHobbies.add(hobby));
        user.hobbies = Array.from(existingHobbies);
    }
};
